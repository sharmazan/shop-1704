from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect

from .forms import ContactForm

# Create your views here.
def index(request):
    return HttpResponse('Hello, world!<br>\
                        <a href="/shop/">Go to shop</a>')

def contact(request):
    # print(request.GET, request.POST)
    # print(request.GET.get('name'))
    # return HttpResponse("Наши контакты - i@itman.in. <br> Спасибо, "+request.GET.get('name'))
    if request.GET:
    # if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ContactForm(request.GET)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponse('Thanks!')
            # return HttpResponseRedirect('/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ContactForm()

    return render(request, 'pages/contact.html', {'form': form})
