from django.db import models
from django.contrib.auth.models import User


class Owner(models.Model):
    phone = models.CharField(max_length=20)
    # photo = models.ImageField()
    user = models.OneToOneField(User)

    def __str__(self):
        return self.user.username


# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=100)
    likes = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "categories"


class Product(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(null=True, blank=True)
    price = models.PositiveIntegerField()
    company = models.ForeignKey('Company')
    category = models.ForeignKey(Category)

    def __str__(self):
        return self.title


class Company(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    contacts = models.TextField()
    owner = models.OneToOneField(Owner)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "companies"
