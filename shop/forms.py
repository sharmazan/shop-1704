from django.forms import ModelForm, PasswordInput, CharField
from django.contrib.auth.models import User
from .models import Product, Company, Owner

class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = '__all__'

class CompanyForm(ModelForm):
    class Meta:
        model = Company
        exclude = ('owner',)

class OwnerForm(ModelForm):
    class Meta:
        model = Owner
        fields = ('phone',)

class UserForm(ModelForm):
    password = CharField(widget=PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')
