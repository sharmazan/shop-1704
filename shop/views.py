from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login
from django.db.models import F

from .models import Category, Company, Product
from .forms import ProductForm, CompanyForm, UserForm, OwnerForm
# Create your views here.

def single_product(request, product_id):
    # product = Product.objects.get(pk=product_id)
    product = get_object_or_404(Product, pk=product_id)
    cat = product.category
    company = product.company

    print(product.id, product.title)

    # return HttpResponse(product.title+' in '+cat.title)
    return render(request, 'shop/product.html', {'product': product})


def products(request):
    if request.GET:
        print(request.GET['s'])
    s = request.GET.get('s', '')
    if s:
        products = Product.objects.filter(title__icontains=s)
    else:
        products = Product.objects.all()
    # print(products)
    # return HttpResponse('Продукты в количестве '+str(len(products)))
    return render(request, 'shop/products.html', {'product_list': products})


def new_product(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ProductForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            # return HttpResponse('Thanks!')
            form.save()
            return HttpResponseRedirect('/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = ProductForm()

    return render(request, 'shop/newproduct.html', {'form': form})


def category(request, cat_id):
    cat = Category.objects.get(pk=cat_id)
    products = Product.objects.filter(category__id=cat_id)
    print(products)

    return render(request, 'shop/category.html', {'category': cat, 'product_list': products})


@login_required
def like_category_woajax(request):
    cat_id = request.GET.get('category_id')
    cat = Category.objects.get(pk=cat_id)
    cat.likes = F('likes') + 1
    cat.save(update_fields=['likes'])
    print(cat.likes)
    return HttpResponseRedirect(reverse('category', args=[cat.id]))


@login_required
def like_category(request):
    cat_id = request.GET.get('category_id')
    cat = Category.objects.get(pk=cat_id)
    cat.likes = F('likes') + 1
    cat.save(update_fields=['likes'])
    cat.refresh_from_db()
    print('category_id', cat.likes)
    # return HttpResponseRedirect(reverse('category', args=[cat.id]))
    if request.GET.get('api'):
        return HttpResponse(cat.likes)
    else:
        return HttpResponseRedirect(reverse('category', args=[cat.id]))
    


@login_required
def new_company(request):
    print(request.user)
    print(request.user.owner)
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = CompanyForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            # return HttpResponse('Thanks!')
            company = form.save(commit=False)
            company.owner = request.user.owner
            company.save()
            return HttpResponseRedirect('/shop/')

    # if a GET (or any other method) we'll create a blank form
    else:
        form = CompanyForm()

    return render(request, 'shop/newcompany.html', {'form': form})



def new_user(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        userform = UserForm(request.POST)
        ownerform = OwnerForm(request.POST)

        # check whether it's valid:
        if userform.is_valid() and ownerform.is_valid():
            user = userform.save()
            user.set_password(user.password)
            user.save()
            owner = ownerform.save(commit=False)
            owner.user = user
            owner.save()
            login(request, user)
            return HttpResponseRedirect(reverse('new_company'))

    # if a GET (or any other method) we'll create a blank form
    else:
        userform = UserForm()
        ownerform = OwnerForm()

    return render(request, 'shop/newuser.html', {'userform': userform,
                                                'ownerform': ownerform, })
