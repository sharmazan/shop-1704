"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from pages import views
from shop.views import single_product, products, category, new_product, new_company, new_user, like_category

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url('^', include('django.contrib.auth.urls')),
    url(r'^$', views.index, name='home'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^shop/$', products, name='products'),
    url(r'^shop/add/$', new_product, name='new_product'),
    url(r'^shop/addcompany/$', new_company, name='new_company'),
    url(r'^shop/addowner/$', new_user, name='new_user'),
    url(r'^shop/(?P<product_id>[0-9]+)/$', single_product, name='single_product'),
    url(r'^shop/category/(?P<cat_id>[0-9]+)/$', category, name='category'),
    url(r'^like_category/$', like_category, name='like_category'),
    
]
